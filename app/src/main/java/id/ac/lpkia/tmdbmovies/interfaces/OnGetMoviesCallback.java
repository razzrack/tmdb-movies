package id.ac.lpkia.tmdbmovies.interfaces;

import java.util.List;
import id.ac.lpkia.tmdbmovies.model.Movie;

public interface OnGetMoviesCallback {
    void onSuccess(int page, List<Movie> movies);

    void onError();
}
