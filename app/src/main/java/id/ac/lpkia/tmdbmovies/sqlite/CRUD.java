package id.ac.lpkia.tmdbmovies.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import id.ac.lpkia.tmdbmovies.api.UserRequest;

public class CRUD extends SQLiteOpenHelper {
    Context context;

    public CRUD(Context context) {
        super(context, Constant.DATABASE_NAME, null, Constant.DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlCreateTable = "CREATE TABLE IF NOT EXISTS " + Constant.DATABASE_TABLE
                + "(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Constant.KEY_NRP + " TEXT (50) NOT NULL,"
                + Constant.KEY_FULL_NAME + " TEXT (100),"
                + Constant.KEY_EMAIL + " TEXT (100),"
                + Constant.KEY_PASSWORD + " TEXT (100),"
                + Constant.KEY_CLASSES + " TEXT (100))";

        db.execSQL(sqlCreateTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sqlDrop = "DROP TABLE IF EXISTS " + Constant.DATABASE_TABLE;
        String sqlCreateTable = "CREATE TABLE IF NOT EXISTS " + Constant.DATABASE_TABLE
                + "(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Constant.KEY_NRP + " TEXT (50) NOT NULL,"
                + Constant.KEY_FULL_NAME + " TEXT (100),"
                + Constant.KEY_EMAIL + " TEXT (100),"
                + Constant.KEY_PASSWORD + " TEXT (100),"
                + Constant.KEY_CLASSES + " TEXT (100))";

        db.execSQL(sqlDrop);
        db.execSQL(sqlCreateTable);
        onCreate(db);
    }

    public void create(UserRequest userRequest){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Constant.KEY_NRP, userRequest.getNrp());
        values.put(Constant.KEY_FULL_NAME, userRequest.getFullname());
        values.put(Constant.KEY_EMAIL, userRequest.getEmail());
        values.put(Constant.KEY_PASSWORD, userRequest.getPassword());
        values.put(Constant.KEY_CLASSES, userRequest.getClasses());
        db.insert(Constant.DATABASE_TABLE,null,values);
        db.close();
    }

    public void update(UserRequest userRequest) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Constant.KEY_NRP, userRequest.getNrp());
        values.put(Constant.KEY_FULL_NAME, userRequest.getFullname());
        values.put(Constant.KEY_EMAIL, userRequest.getEmail());
        values.put(Constant.KEY_PASSWORD, userRequest.getPassword());
        values.put(Constant.KEY_CLASSES, userRequest.getClasses());
        db.update(Constant.DATABASE_TABLE, values, Constant.KEY_NRP + "= ?", new String[]{userRequest.getNrp()});
        db.close();
    }

    public ArrayList<UserRequest> selectAll() {
        ArrayList<UserRequest> userRequests = new ArrayList<>();
        String selectAllQuery = "SELECT * FROM " + Constant.DATABASE_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectAllQuery, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String nrp = cursor.getString(1);
                String fullname = cursor.getString(2);
                String email = cursor.getString(3);
                String password = cursor.getString(4);
                String group = cursor.getString(5);
                String classes = cursor.getString(6);
                UserRequest userRequest = new UserRequest(nrp, fullname, email, password, group, classes);
                userRequests.add(userRequest);
                cursor.moveToNext();
            }
        }
        return userRequests;
    }
}
