package id.ac.lpkia.tmdbmovies.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.ac.lpkia.tmdbmovies.R;
import id.ac.lpkia.tmdbmovies.activity.UserActivity;
import id.ac.lpkia.tmdbmovies.model.User;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    UserActivity ctx;
    private List<User> userList;

    Context mContext;

    public UserAdapter(List<User> userList) {
        this.userList = userList;
    }

    @NonNull
    @Override
    public UserAdapter.UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.activity_user, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapter.UserViewHolder holder, final int position) {
        holder.tvNrp.setText(userList.get(position).getNrp());
        holder.tvFullname.setText(userList.get(position).getFullname());
        holder.tvEmail.setText(userList.get(position).getEmail());
        holder.tvPassword.setText(userList.get(position).getPassword());
        holder.tvGroup.setText(userList.get(position).getGroup());
        holder.tvClasses.setText(userList.get(position).getClasses());
    }

    @Override
    public int getItemCount() {
        return (userList != null) ? userList.size() : 0;
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNrp, tvFullname, tvEmail, tvPassword, tvGroup, tvClasses;
        LinearLayoutCompat layoutRoot;

        public UserViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNrp = itemView.findViewById(R.id.tvNrp);
            tvFullname = itemView.findViewById(R.id.tvFullname);
            tvEmail = itemView.findViewById(R.id.tvEmail);
            tvPassword = itemView.findViewById(R.id.tvPassword);
            tvGroup = itemView.findViewById(R.id.tvGroup);
            tvClasses = itemView.findViewById(R.id.tvClasses);
        }
    }
}
