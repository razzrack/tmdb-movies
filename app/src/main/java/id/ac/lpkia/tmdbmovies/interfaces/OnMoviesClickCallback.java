package id.ac.lpkia.tmdbmovies.interfaces;

import id.ac.lpkia.tmdbmovies.model.Movie;

public interface OnMoviesClickCallback {
    void onClick(Movie movie);
}
