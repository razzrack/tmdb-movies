package id.ac.lpkia.tmdbmovies.interfaces;

import java.util.List;

import id.ac.lpkia.tmdbmovies.model.Genre;

public interface OnGetGenresCallback {

    void onSuccess(List<Genre> genres);

    void onError();
}
