package id.ac.lpkia.tmdbmovies.api;

import id.ac.lpkia.tmdbmovies.interfaces.UserService;

public class UtilsApi {
    public static final String BASE_URL_API = "https://obscure-reaches-88394.herokuapp.com/api/v1/";

    // deklarasi Interface UserService
    public static UserService getAPIService(){
        return ApiClient.getClient(BASE_URL_API).create(UserService.class);
    }
}