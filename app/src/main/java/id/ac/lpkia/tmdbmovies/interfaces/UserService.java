package id.ac.lpkia.tmdbmovies.interfaces;

import java.util.List;

import id.ac.lpkia.tmdbmovies.api.UserRequest;
import id.ac.lpkia.tmdbmovies.api.UserResponse;
import id.ac.lpkia.tmdbmovies.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface UserService {
    @POST("users/signIn")
    Call<UserResponse> userLogin(@Body UserRequest userRequest);

    @POST("users")
    Call<UserResponse> userRegister(@Body UserRequest userRequest);

    @GET("users")
    Call<List<User>> getUser();

    @GET("users/{id}")
    Call<User> getUserByID();

}
