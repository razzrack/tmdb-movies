package id.ac.lpkia.tmdbmovies.sqlite;

public class Constant {
    public static final Integer DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "tmdb";
    public static final String DATABASE_TABLE = "user";
    public static final String KEY_NRP = "nrp";
    public static final String KEY_FULL_NAME = "fullname";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_CLASSES = "classes";
}
