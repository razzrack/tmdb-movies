package id.ac.lpkia.tmdbmovies.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import id.ac.lpkia.tmdbmovies.R;
import id.ac.lpkia.tmdbmovies.api.UserRequest;
import id.ac.lpkia.tmdbmovies.api.UserResponse;
import id.ac.lpkia.tmdbmovies.interfaces.UserService;
import id.ac.lpkia.tmdbmovies.api.UtilsApi;
import id.ac.lpkia.tmdbmovies.sqlite.CRUD;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {
    EditText etNrp, etFullname, etEmail, etPassword, etGroup, etClasses;

    Context mContext;
    UserService mUserService;

    boolean isUpdate;
    CRUD crud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        crud = new CRUD(this);

        mContext = this;
        mUserService = UtilsApi.getAPIService(); // inisialisasi yang ada di package apihelper

        initComponents();
    }

    private void initComponents() {
        etNrp = findViewById(R.id.etNrp);
        etFullname = findViewById(R.id.etFullname);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etGroup = findViewById(R.id.etGroup);
        etClasses = findViewById(R.id.etClasses);

        isUpdate = getIntent().getBooleanExtra("update", false);
        if (isUpdate) {
            String nrp = getIntent().getStringExtra("nrp");
            String fullname = getIntent().getStringExtra("fullname");
            String email = getIntent().getStringExtra("email");
            String password = getIntent().getStringExtra("password");
            String group = getIntent().getStringExtra("group");
            String classes = getIntent().getStringExtra("classes");
            etNrp.setText(nrp);
            etNrp.setEnabled(false);
            etFullname.setText(fullname);
            etEmail.setText(email);
            etPassword.setText(password);
            etGroup.setText(group);
            etClasses.setText(classes);
        }

        findViewById(R.id.btnRegistration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestRegister();
            }
        });
    }

    private void requestRegister(){
        final UserRequest userRequest = new UserRequest();
        userRequest.setNrp(etNrp.getText().toString());
        userRequest.setFullname(etFullname.getText().toString());
        userRequest.setEmail(etEmail.getText().toString());
        userRequest.setPassword(etPassword.getText().toString());
        userRequest.setGroup(etGroup.getText().toString());
        userRequest.setClasses(etClasses.getText().toString());

        if (isUpdate) {
            crud.update(userRequest);
        } else {
            crud.create(userRequest);
        }
        setResult(201);
        onBackPressed();

        Call<UserResponse> registerResponseCall = UtilsApi.getAPIService().userRegister(userRequest);
        registerResponseCall.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.isSuccessful()){
                    Toast.makeText(RegistrationActivity.this, "User Berhasil Ditambahkan", Toast.LENGTH_LONG).show();
                    final UserResponse userResponse = response.body();
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(RegistrationActivity.this, "Login Gagal Ditambahkan", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Toast.makeText(RegistrationActivity.this, "Throwable " +t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
