package id.ac.lpkia.tmdbmovies.interfaces;

import id.ac.lpkia.tmdbmovies.model.Movie;

public interface OnGetMovieCallback {

    void onSuccess(Movie movie);

    void onError();
}
