package id.ac.lpkia.tmdbmovies.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import id.ac.lpkia.tmdbmovies.R;
import id.ac.lpkia.tmdbmovies.adapter.UserAdapter;
import id.ac.lpkia.tmdbmovies.interfaces.UserService;
import id.ac.lpkia.tmdbmovies.api.UtilsApi;
import id.ac.lpkia.tmdbmovies.model.User;

public class UserActivity extends AppCompatActivity {
    ListView listView;
    RecyclerView rvUser;
    UserService userService;
    List<User> list = new ArrayList<User>();
    UserAdapter userAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        rvUser = findViewById(R.id.rvUser);
        userService = UtilsApi.getAPIService();

        getUserList();
    }

    private void getUserList() {
//        Call<List<User>> call = userService.getUser();
//        call.enqueue(new Callback<List<User>>() {
//            @Override
//            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
//                if (response.isSuccessful()){
//                    list = response.body();
//                    rvUser.setAdapter(new UserAdapter());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<User>> call, Throwable t) {
//
//            }
//        });
    }
}
