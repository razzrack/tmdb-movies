package id.ac.lpkia.tmdbmovies.interfaces;

import java.util.List;
import id.ac.lpkia.tmdbmovies.model.Review;

public interface OnGetReviewsCallback {
    void onSuccess(List<Review> reviews);

    void onError();
}
