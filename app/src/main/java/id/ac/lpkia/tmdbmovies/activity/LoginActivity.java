package id.ac.lpkia.tmdbmovies.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import id.ac.lpkia.tmdbmovies.R;
import id.ac.lpkia.tmdbmovies.api.UserRequest;
import id.ac.lpkia.tmdbmovies.api.UserResponse;
import id.ac.lpkia.tmdbmovies.interfaces.UserService;
import id.ac.lpkia.tmdbmovies.api.UtilsApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    EditText nrp, password;

    Context mContext;
    UserService mUserService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = this;
        mUserService = UtilsApi.getAPIService(); // inisialisasi yang ada di package apihelper


        initComponents();
    }

    private void initComponents() {
        nrp = findViewById(R.id.etNrp);
        password = findViewById(R.id.etPassword);

        findViewById(R.id.tvRegistration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, RegistrationActivity.class));
            }
        });

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(nrp.getText().toString())) {
                    Toast.makeText(LoginActivity.this, "NRP tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(password.getText().toString())) {
                    Toast.makeText(LoginActivity.this, "Password tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                } else {
                    // login berhasil
                    requestLogin();
                }

            }
        });
    }

    public void requestLogin() {
        final UserRequest userRequest = new UserRequest();
        userRequest.setNrp(nrp.getText().toString());
        userRequest.setPassword(password.getText().toString());

        Call<UserResponse> loginResponseCall = UtilsApi.getAPIService().userLogin(userRequest);
        loginResponseCall.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, final Response<UserResponse> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(LoginActivity.this, "Login Berhasil", Toast.LENGTH_LONG).show();
                    final UserResponse userResponse = response.body();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(mContext, HomeActivity.class);
                            intent.putExtra("data", nrp.getText().toString());
                            startActivity(intent);
                        }
                    }, 700);
                } else {
                    Toast.makeText(LoginActivity.this, "Login Gagal", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Throwable " +t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
