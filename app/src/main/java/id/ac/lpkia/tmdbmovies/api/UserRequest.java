package id.ac.lpkia.tmdbmovies.api;

public class UserRequest {
    private String nrp;
    private String fullname;
    private String email;
    private String password;
    private String group;
    private String classes;

    public UserRequest(String nrp, String fullname, String email, String password, String group, String classes) {
        this.nrp = nrp;
        this.fullname = fullname;
        this.email = email;
        this.password = password;
        this.group = group;
        this.classes = classes;
    }

    public UserRequest() {

    }

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getClasses() {
        return classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }
}
