package id.ac.lpkia.tmdbmovies.interfaces;

import java.util.List;
import id.ac.lpkia.tmdbmovies.model.Trailer;

public interface OnGetTrailersCallback {
    void onSuccess(List<Trailer> trailers);

    void onError();
}
